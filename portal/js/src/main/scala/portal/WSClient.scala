package portal

import scala.concurrent._

import autowire._
import io.circe._
import io.circe.syntax._
import monix.execution._
import monix.eval._
import org.scalajs.dom

object WSClient extends autowire.Client[String, Decoder, Encoder] {
  implicit val scheduler = Scheduler()
  val ws = new dom.WebSocket(s"ws://${dom.document.location.host}/api")
  val wsopen = Promise[Unit]()
  ws.onopen = { _ => wsopen.success(()) }

  def write[Result: Encoder](r: Result): String =
    r.asJson.noSpaces

  def read[Result: Decoder](s: String): Result =
    json.read[Result](s)

  def doCall(req: Request): Future[String] = Task.create[String] { (scheduler, callback) =>
    val apiReq = ApiRequest(req.path, req.args)
    ws.onerror =  { error: dom.ErrorEvent => callback.onError(new Exception(error.message)) }
    ws.onmessage = { message: dom.MessageEvent => callback.onSuccess(message.data.asInstanceOf[String]) }
    wsopen.future.foreach(_ => ws.send(apiReq.asJson.noSpaces))(scheduler)
    Cancelable.empty
  }.runAsync
}
