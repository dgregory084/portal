package portal

import scala.concurrent._
import scala.concurrent.duration._
import scalajs.js.JSApp

import autowire._
import monix.execution._
import monix.eval._
import monix.reactive._

object UI extends JSApp {
  implicit val scheduler = Scheduler()

  def main(): Unit = {
    Observable.interval(1.second)
      .mapFuture(_ => WSClient[Api].getActiveJobs().call())
      .map(list => list.length)
      .dump("getActiveJobs()")
      .subscribe()
  }
}
