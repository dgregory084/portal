package portal

import io.circe._
import io.circe.generic.semiauto._

sealed trait JobType
case object Autostart extends JobType
case object Batch extends JobType
case object BatchImmediate extends JobType
case object ProcedureStartRequest extends JobType
case object Interactive extends JobType
case object Advanced36Server extends JobType
case object MultipleRequester extends JobType
case object Prestart extends JobType
case object PrintDriver extends JobType
case object SpoolReader extends JobType
case object SubsystemMonitor extends JobType
case object System extends JobType
case object SpoolWriter extends JobType

object JobType {
  implicit val encoder: Encoder[JobType] = deriveEncoder[JobType]
  implicit val decoder: Decoder[JobType] = deriveDecoder[JobType]
}

case class JobName(number: Int, user: String, name: String)

object JobName {
  implicit val encoder: Encoder[JobName] = deriveEncoder[JobName]
  implicit val decoder: Decoder[JobName] = deriveDecoder[JobName]
}

case class ActiveJob(
  name: JobName,
  subsystem: Option[String],
  jobType: JobType,
  status: String,
  priority: Int,
  threadCount: Int,
  cpuPercentage: BigDecimal
)

object ActiveJob {
  implicit val encoder: Encoder[ActiveJob] = deriveEncoder[ActiveJob]
  implicit val decoder: Decoder[ActiveJob] = deriveDecoder[ActiveJob]
}

case class ApiRequest(path: Seq[String], args: Map[String, String])

object ApiRequest {
  implicit val encoder: Encoder[ApiRequest] = deriveEncoder[ApiRequest]
  implicit val decoder: Decoder[ApiRequest] = deriveDecoder[ApiRequest]
}
