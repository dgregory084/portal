import io.circe._
import io.circe.syntax._

package object portal {
  object json {
    def read[A: Decoder](str: String): A = (for {
      json <- parser.parse(str)
      res <- json.as[A]
    } yield res).right.get
  }
}
