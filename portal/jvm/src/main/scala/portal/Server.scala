package portal

import scala.concurrent._

import autowire._
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.ws._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import io.circe._
import io.circe.syntax._

object Server extends Api {

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("portal-actor-system")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val router = WSServer.route[Api](Server)

    def runRequest(req: String): Future[TextMessage] = {
      val ApiRequest(path, args) = json.read[ApiRequest](req)
      val transaction = router(autowire.Core.Request(path, args))
      transaction.map(TextMessage.apply)
    }

    val messageFlow = Flow[Message].collect {
      case text: TextMessage => text
    }.flatMapConcat {
      _.textStream.flatMapConcat { req =>
        Source.fromFuture(runRequest(req))
      }
    }

    val route = path("api") {
      handleWebSocketMessages(messageFlow)
    } ~ pathSingleSlash {
      get { complete(Page.index) }
    } ~ getFromResourceDirectory("")

    Http().bindAndHandle(route, Config.host, Config.port)
  }

  def getActiveJobs(): List[ActiveJob] = Database.getActiveJobs()
}
