package portal
package db

import com.zaxxer.hikari._
import java.lang.Runtime
import sqlest._

object ConnectionPool {
  val database = {
    val BlankParam = " " * 30
    val hikariConfig = new HikariConfig
    hikariConfig.setDriverClassName("com.ibm.as400.access.AS400JDBCDriver")
    hikariConfig.setJdbcUrl("jdbc:as400://" + Config.iseries)
    hikariConfig.setUsername(Config.username)
    hikariConfig.setPassword(Config.password)
    hikariConfig.setConnectionTestQuery("values(0)")
    hikariConfig.addDataSourceProperty("naming", "system")
    hikariConfig.addDataSourceProperty("prompt", "false")
    hikariConfig.setConnectionInitSql(
      "call envconfig/env003acl(" +
      s"'SERVERAPP ', '$BlankParam', " +
      s"'$BlankParam', '$BlankParam', " +
      f"'${Config.environment}%-10s')"
    )
    val dataSource = new HikariDataSource(hikariConfig)
    Runtime.getRuntime.addShutdownHook(new Thread(new Runnable {
      def run() = dataSource.close
    }))
    sqlest.Database.withDataSource(dataSource, sqlest.sql.DB2StatementBuilder)
  }
}
