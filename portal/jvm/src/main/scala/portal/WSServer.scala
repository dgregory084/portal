package portal

import autowire._
import io.circe._
import io.circe.syntax._

object WSServer extends autowire.Server[String, Decoder, Encoder] {
  def write[Result: Encoder](r: Result): String =
    r.asJson.noSpaces
  def read[Result: Decoder](s: String): Result =
    json.read[Result](s)
}
