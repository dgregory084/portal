package portal

import com.typesafe.config.ConfigFactory

object Config {
  private val typesafeConfig = ConfigFactory.load
  val iseries = typesafeConfig.getString("iseries.host")
  val environment = typesafeConfig.getString("iseries.environment")
  val username = typesafeConfig.getString("iseries.username")
  val password = typesafeConfig.getString("iseries.password")
  val host = typesafeConfig.getString("server.host")
  val port = typesafeConfig.getInt("server.port")
}
