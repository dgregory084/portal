package portal

import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import scalatags.Text.all._

object Page {
  val optimizedJS = if (PortalBuild.isSnapshot) "fastopt" else "fullopt"

  def index = HttpEntity(
    ContentTypes.`text/html(UTF-8)`,
    html(
      header(script(src:= s"/portal-${optimizedJS}.js")),
      body("Hello world")
    ).render
  )
}
