package portal

import portal.db._
import sqlest._

object ActiveJobsTable extends sqlest.ast.BaseTable {
  val tableName = "qsys2.active_job_info"
  val aliasedAs = Some("active_job_info")

  val JobNameColumnType = MappedColumnType({ jobName: String =>
    val fst = jobName.indexOf('/')
    val lst = jobName.lastIndexOf('/')
    JobName(jobName.take(6).toInt, jobName.substring(fst + 1, lst), jobName.substring(lst + 1))
  }, { jobName: JobName =>
    s"${jobName.number}/${jobName.user}/${jobName.name}"
  })

  val JobTypeColumnType = EnumerationColumnType[JobType, String](
    Autostart -> "ASJ",
    Batch -> "BCH",
    BatchImmediate -> "BCI",
    ProcedureStartRequest -> "EVK",
    Interactive -> "INT",
    Advanced36Server -> "M36",
    MultipleRequester -> "MRT",
    Prestart -> "PJ",
    PrintDriver -> "PDJ",
    SpoolReader -> "RDR",
    SubsystemMonitor -> "SBS",
    System -> "SYS",
    SpoolWriter -> "WTR"
  )

  val ordinalPosition = column[Int]("ordinal_position")
  val jobName = column[JobName]("job_name")(JobNameColumnType)
  val subsystem = column[Option[String]]("subsystem")
  val jobType = column[JobType]("job_type")(JobTypeColumnType)
  val status = column[String]("job_status")
  val priority = column[Int]("run_priority")
  val threadCount = column[Int]("thread_count")
  val cpuPercentage = column[BigDecimal]("elapsed_cpu_percentage")
}

object Database {
  implicit val database = ConnectionPool.database

  val activeJobsTable = sqlest.ast.TableFunctionApplication(
    ActiveJobsTable.tableName,
    ActiveJobsTable.aliasedAs,
    Seq.empty,
    ActiveJobsTable
  )

  val activeJobExtractor = extract[ActiveJob](
    name = ActiveJobsTable.jobName,
    subsystem = ActiveJobsTable.subsystem,
    jobType = ActiveJobsTable.jobType,
    status = ActiveJobsTable.status,
    priority = ActiveJobsTable.priority,
    threadCount = ActiveJobsTable.threadCount,
    cpuPercentage = ActiveJobsTable.cpuPercentage
  )

  def getActiveJobs() = select
    .from(activeJobsTable)
    .extractAll(activeJobExtractor)
}
