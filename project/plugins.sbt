resolvers += "JHC releases" at "http://nexus.jhc.co.uk/nexus/content/repositories/releases/"

addSbtPlugin("uk.co.jhc" % "jhc-sbt-plugins" % "0.1.0")

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.15")

addSbtPlugin("io.spray" % "sbt-revolver" % "0.8.0")

addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.6.1")
