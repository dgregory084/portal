lazy val root = (project in file("."))
  .aggregate(portalJS, portalJVM)

val circeVersion = "0.7.0"
val monixVersion = "2.2.4"
val sloggingVersion = "0.5.2"
val scalaCssVersion = "0.5.1"

lazy val portal = crossProject
  .enablePlugins(BuildInfoPlugin)
  .settings(
    scalaVersion := "2.12.0",

    buildInfoKeys := Seq(version, isSnapshot),
    buildInfoPackage := "portal",
    buildInfoObject := "PortalBuild",

    // scalacOptions += "-Xlog-implicits",

    libraryDependencies ++= Seq(
      "io.circe" %%% "circe-core",
      "io.circe" %%% "circe-generic",
      "io.circe" %%% "circe-parser"
    ).map(_ % circeVersion),

    libraryDependencies ++= Seq(
      "org.typelevel" %%% "cats-core" % "0.9.0",
      "io.monix" %%% "monix" % monixVersion,
      "io.monix" %%% "monix-cats" % monixVersion,
      "com.lihaoyi" %%% "autowire" % "0.2.6",
      "com.lihaoyi" %%% "scalatags" % "0.6.3",
      "com.github.japgolly.scalacss" %%% "core" % scalaCssVersion,
      "com.github.japgolly.scalacss" %%% "ext-scalatags" % scalaCssVersion,
      "biz.enef" %%% "slogging" % sloggingVersion,
      "org.scalatest" %%% "scalatest" % "3.0.1" % Test
    )
  )

lazy val portalJS = portal.js.settings(
  scalaJSUseMainModuleInitializer := true
)

lazy val portalJVM = portal.jvm.settings(
  resources in Compile += Def.taskDyn {
    if (isSnapshot.value)
      (fastOptJS in (portalJS, Compile))
    else
      (fullOptJS in (portalJS, Compile))
  }.value.data,
  libraryDependencies ++= Seq(
    "com.typesafe.akka" %% "akka-http" % "10.0.5",
    "com.typesafe" % "config" % "1.3.0",
    "com.zaxxer" % "HikariCP" % "2.4.3",
    "uk.co.jhc" %% "sqlest" % "0.8.8",
    "net.sf.jt400" % "jt400" % "9.2",
    "biz.enef" %% "slogging-slf4j" % sloggingVersion,
    "ch.qos.logback" % "logback-classic" % "1.1.3"
  )
)
